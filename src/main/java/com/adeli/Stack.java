package com.adeli;

import java.util.*;
import java.util.Queue;

public class Stack<T> {
    private int top;
    private List<T> holder;
    private int capacity;

    public Stack(int capacity) {
        top = -1;
        this.capacity = capacity;
        holder = new ArrayList<>(capacity);
    }

    public T push(T t) {
        if (top >= capacity)
            throw new StackOverflowError();
        else {
            holder.add(t);
            top++;
        }
        return t;
    }

    public T pop() {
        T t = peek();
        holder.remove(top--);
        return t;
    }

    public T peek() {
        if (top < 0) {
            throw new EmptyStackException();
        } else {
            return holder.get(top);
        }
    }

    public boolean isEmpty() {
        return top < 0;
    }

    public int getSize() {
        return holder.size();
    }

    public int getCapacity() {
        return capacity;
    }

}
