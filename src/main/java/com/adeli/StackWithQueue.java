package com.adeli;

import java.util.EmptyStackException;

public class StackWithQueue<T> {
    private int size;
    private Queue<T> temp;
    private int capacity;

    public StackWithQueue(int capacity) {
        size = 0;
        this.capacity = capacity;
        temp = new Queue<>(capacity);
    }

    public T push(T t) throws Exception {
        if (temp.isFull())
            throw new StackOverflowError();
        else {
            temp.enqueue(t);
            size++;
        }
        return t;
    }

    public T pop() throws Exception {
        for (int i = 0; i < size - 1; i++) {
            temp.enqueue(temp.dequeue());
        }
        T t = temp.dequeue();
        size--;
        return t;
    }

    public T peek() throws Exception {
        if (isEmpty()) {
            throw new EmptyStackException();
        } else {
            return temp.rear();
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return capacity;
    }
}
