package com.adeli;

import java.util.ArrayList;
import java.util.List;

/**
 * @param <T>
 */
public class Queue<T> {
    int rear, front, size;
    int capacity;
    List<T> holder;

    /**
     * @param capacity
     */
    public Queue(int capacity) {
        this.capacity = capacity;
        size = 0;
        front = -1;
        rear = -1;
        holder = new ArrayList<>(capacity);
    }

    /**
     * @return
     */
    public boolean isFull() {
        return (size == capacity);
    }

    /**
     * @return
     */
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * @param item
     * @throws Exception
     */
    public void enqueue(T item) throws Exception {
        if (isFull())
            throw new Exception("Queue is Full.");
        holder.add(item);
        rear++;
        size++;
    }

    /**
     * @return
     * @throws Exception
     */
    public T dequeue() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        T item = holder.remove(0);
        rear--;
        size--;
        return item;
    }

    /**
     * @return
     */
    public T front() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        return holder.get(0);
    }

    /**
     * @return
     */
    public T rear() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        return holder.get(rear);
    }

}
