package com.adeli;

/**
 * @param <T>
 */
public class QueueWithStack<T> {
    private Stack<T> tempIn;
    private Stack<T> tempOut;
    private int capacity;
    private int size;

    /**
     * @param capacity
     */
    public QueueWithStack(int capacity) {
        tempIn = new Stack<>(capacity);
        tempOut = new Stack<>(capacity);
        this.capacity = capacity;
        size = 0;
    }

    /**
     * @return
     */
    public boolean isFull() {
        return (size == capacity);
    }

    /**
     * @return
     */
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * @param item
     * @throws Exception
     */
    public void enqueue(T item) throws Exception {
        if (isFull())
            throw new Exception("Queue is Full.");
        if (tempIn.isEmpty())
            changeTemp();
        tempIn.push(item);
        size++;
    }

    /**
     * @return
     * @throws Exception
     */
    public T dequeue() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        if (tempOut.isEmpty())
            changeTemp();
        T item = tempOut.pop();
        size--;
        return item;
    }

    private void changeTemp() {
        if (tempIn.isEmpty())
            for (int i = 0; i < size; i++)
                tempIn.push(tempOut.pop());
        else
            for (int i = 0; i < size; i++)
                tempOut.push(tempIn.pop());
    }

    /**
     * @return
     */
    public T front() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        if (tempOut.isEmpty()) {
            changeTemp();
        }
        return tempOut.peek();
    }

    /**
     * @return
     */
    public T rear() throws Exception {
        if (isEmpty())
            throw new Exception("Queue is Empty.");
        if (tempIn.isEmpty()) {
            changeTemp();
        }
        return tempIn.peek();
    }
    
}
